/** This file contains examples of iteration over some data structures.
please note that it can't be compiled, so please keep that in mind.
The idea of this file is to be printed and used as an example when needed. **/

// An example of a recursive iteration over a binray tree. In this case I calculate the sum of the tree, it could be easily changed.
// Modify the "+" sign if asked to do something else.
public static int getTreeSum(binTree<int> t){
  // I define a sum variable that contains the sum of the tree.
  int sum = 0;
  // First, add the current value of the current location in the tree,
  sum += t.getValue();
  if t.hasRight(){
    // If the tree hasRight, I call the function with the right "son" as the parameter.
    // This starts the recursive iteration over the whole tree.
    sum += getTreeSum(t.getRight());
  }
  if t.hasLeft(){
    // Same as above only with the left son
    sum += getTreeSum(t.getLeft());
  }
  // Eventually I am left with the total sum of the tree to return.
  return sum;
}


public static int getTreeSize(binTree<int> t){
  int size = 0;
  if t != null{
    size += 1;
  }
  else{
    if t.hasRight(){
      // If the tree hasRight, I call the function with the right "son" as the parameter.
      // This starts the recursive iteration over the whole tree.
      size += getTreeSum(t.getRight());
    }
    if t.hasLeft(){
      // Same as above only with the left son
      size += getTreeSum(t.getLeft());
    }
  }
  return size
}

public static void returnAllTreeValues(binTree<int> t){
  List l = new List();
  if t != null{
    l.add(t.getValue());
  }
  if t.hasRight(){
    // If the tree hasRight, I call the function with the right "son" as the parameter.
    // This starts the recursive iteration over the whole tree.
    l.add(getTreeSum(t.getRight()));
  }
  if t.hasLeft(){
    // Same as above only with the left son
    l.add(getTreeSum(t.getLeft()));
  }
}
